<?php
use TopFloor\Cds\CdsCaches\CdsCache;

/**
 * Created by PhpStorm.
 * User: BMcClure
 * Date: 1/9/2016
 * Time: 3:51 PM
 */
class DrupalCdsCache extends CdsCache {
  static $empty = null;

  public function get($key, $permanent = false) {
    $data = &drupal_static('DrupalCdsCache');
    if (!isset($data)) {
      $data = array();
    }

    if (!isset($data[$key])) {
      if ($permanent) {
        $cache = variable_get('cds_data-' . $key, null);

        if (!is_null($cache)) {
          $data[$key] = $cache;
        }
      } else {
        $cache = cache_get('cds_data-' . $key);

        if ($cache) {
          $data[$key] = &$cache->data;
        }
      }

    }

    return isset($data[$key]) ? $data[$key] : null;
  }

  public function set($key, &$value, $permanent = false) {
    $data = &drupal_static('DrupalCdsCache');
    if (!isset($data)) {
      $data = array();
    }

    $data[$key] = &$value;

    if ($permanent) {
      variable_set('cds_data-' . $key, $value);
    } else {
      cache_set('cds_data-' . $key, $value, 'cache');
    }
  }

  public function clear($key = null, $wildcard = true) {
    if (empty($key)) {
      $key = 'cds_data-';
    }

    cache_clear_all($key, 'cache', $wildcard);
  }
}
