<?php
use TopFloor\Cds\CdsConfigs\CdsConfig;
use TopFloor\Cds\Exceptions\CdsServiceException;

/**
 * Created by PhpStorm.
 * User: BMcClure
 * Date: 1/9/2016
 * Time: 11:10 PM
 */
class DrupalCdsConfig extends CdsConfig {
  protected $configKey;
  public static $defaults = array(
    'cdsPath' => null,
    'host' => 'www.product-config.net',
    'domain' => '',
    'unitSystem' => 'english',
    'customScript' => '',
    'customStylesheet' => '',
    'cartThankYouUrl' => '',
    'aliases' => array(
      'category' => '{base}/{category-hierarchy}',
      'product' => '{base}/{category-hierarchy}/{product}',
      'cart' => 'cart',
      'compare' => 'compare',
      'search' => 'search/catalog',
    ),
    'overrides' => array(
      'category' => array(
          'entityType' => '',
          'bundle' => '',
          'fields' => array(
              'headline' => '',
              'description' => '',
              'image' => '',
              'teaser_description' => '',
              'teaser_image' => '',
          ),
      ),
    ),
  );

  public function __construct($configKey = 'cds_integration') {
    $this->configKey = $configKey;

    parent::__construct();
  }

  public static function defaults() {
    $defaults = self::$defaults;

    $defaults['cdsPath'] = libraries_get_path('cds');



    return $defaults;
  }

  public function get($key) {
    $config = &drupal_static(__FUNCTION__);
    if (!isset($config)) {
      $config = variable_get($this->configKey, array());

      $config += self::defaults();
    }

    if (!isset($config[$key])) {
      throw new CdsServiceException("Config key $key not found.");
    }

    $value = $config[$key];
    
    switch ($key) {
      case 'cartThankYouUrl':
        $value = url($value);

        break;
    }

    return $value;
  }

  public function cdsPath() {
    return libraries_get_path('cds') . '/';
  }
}
