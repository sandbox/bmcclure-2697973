<?php
use TopFloor\Cds\CdsUrlHandlers\EnvironmentBasedCdsUrlHandler;

/**
 * Created by PhpStorm.
 * User: BMcClure
 * Date: 1/9/2016
 * Time: 3:54 PM
 */
class DrupalCdsUrlHandler extends EnvironmentBasedCdsUrlHandler {

  protected function getEnvironments() {
    $environments = array();

    if (function_exists('cds_product_section_info')) {
      $sectionInfo = cds_product_section_info($this->service);

      foreach ($sectionInfo as $nid => $product_section) {
        $environments[drupal_get_path_alias("node/$nid")] = $product_section['category'];
      }
    }

    return $environments;
  }
}
