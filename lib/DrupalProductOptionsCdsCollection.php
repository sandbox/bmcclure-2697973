<?php
use TopFloor\Cds\CdsCollections\ProductOptionsCdsCollection;

/**
 * Created by PhpStorm.
 * User: BMcClure
 * Date: 1/10/2016
 * Time: 11:54 PM
 */
class DrupalProductOptionsCdsCollection extends ProductOptionsCdsCollection {
  public function loadData() {
    $perPage = 250;

    $request = $this->service->productsRequest('root', 0, $perPage);
    $results = $request->process();

    $batch = array(
      'operations' => array(),
      'finished' => 'cds_integration_product_batch_finished',
      'title' => t('Import CDS Products'),
      'init_message' => t('Starting to import CDS products...'),
      'progress_message' => t('Processed product group @current out of @total.'),
      'error_message' => t('CDS product import has encountered an error.'),
    );

    $pages = ceil($results['rowCount'] / $perPage);

    for ($i = 0; $i < $pages; $i++) {
      $batch['operations'][] = array('cds_integration_product_batch_process', array($i, $perPage, $this->getCacheKey()));
    }

    batch_set($batch);
    batch_process(current_path());
  }
}
