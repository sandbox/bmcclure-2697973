<?php

function cds_integration_admin_ui_permissions() {
  return array(
    'administer cds integration' => array(
      'title' => t('Administer CDS Integration'),
      'description' => t('Configure the CDS Integration module settings'),
    ),
  );
}

function cds_integration_admin_ui_menu() {
  return array(
    'admin/config/services/cds_integration' => array(
      'title' => 'CDS Integration',
      'description' => 'Configuration for the CDS Integration module.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('cds_integration_admin_ui_form'),
      'access arguments' => array('administer cds integration'),
      'type' => MENU_NORMAL_ITEM,
    )
  );
}

function cds_integration_admin_ui_form($form, &$form_state) {
  $settings = cds_integration_settings();

  $form['general'] = cds_integration_admin_ui_fields_general($settings);

  $form['aliases'] = cds_integration_admin_ui_fields_aliases($settings);

  $form['#tree'] = TRUE;

  $form['overrides'] = cds_integration_admin_ui_fields_overrides($settings);

  $form['product_sections'] = cds_integration_admin_ui_fields_product_sections($settings);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function cds_integration_admin_ui_fields_general($settings) {
  $section = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#description' => t('Configure basic settings used by the CDS Integration module.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $section['host'] = array(
    '#type' => 'textfield',
    '#title' => t('CDS Host'),
    '#description' => t('Enter the "host" value provided by CDS.'),
    '#default_value' => $settings['host'],
    '#size' => 100,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $section['domain'] = array(
    '#type' => 'textfield',
    '#title' => t('CDS Domain'),
    '#description' => t('Enter the "domain" value provided by CDS.'),
    '#default_value' => $settings['domain'],
    '#size' => 100,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $section['unitSystem'] = array(
    '#type' => 'select',
    '#title' => t('Unit System'),
    '#description' => t('Select the type of units to use on the site.'),
    '#options' => array(
      'english' => 'English',
      'metric' => 'Metric',
    ),
    '#default_value' => $settings['unitSystem'],
  );

  $section['customScript'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom Script'),
      '#description' => t('If provided, enter the filename of the custom CDS script for this site.'),
      '#default_value' => $settings['customScript'],
      '#size' => '60',
      '#maxlength' => 255,
      '#required' => FALSE,
  );

  $section['customStylesheet'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom Stylesheet'),
      '#description' => t('If provided, enter the filename of the custom CDS stylesheet for this site.'),
      '#default_value' => $settings['customStylesheet'],
      '#size' => '60',
      '#maxlength' => 255,
      '#required' => FALSE,
  );

  $section['cartThankYouUrl'] = array(
      '#type' => 'textfield',
      '#title' => t('Cart Thank You Path'),
      '#description' => t('If provided, customize the URL of the Thank You page after cart submission'),
      '#default_value' => $settings['cartThankYouUrl'],
      '#size' => '60',
      '#maxlength' => 255,
      '#required' => FALSE,
  );

  return $section;
}

function cds_integration_admin_ui_entity_type_options() {
  $entity_info = entity_get_info();

  $entity_types = array();

  foreach ($entity_info as $name => $data) {
    if ($data['fieldable']) {
      $entity_types[$name] = $data['label'];
    }
  }

  return $entity_types;
}

function cds_integration_admin_ui_bundle_options($entity_type) {
  $entity_types = entity_get_info($entity_type);

  if (!is_null($entity_type)) {
    $entity_types = array($entity_type => $entity_types);
  }

  $bundles = array();

  foreach ((array) $entity_types as $type) {
    if (!$type['fieldable']) {
      continue;
    }

    foreach ($type['bundles'] as $bundle_name => $bundle_data) {
      $bundles[$bundle_name] = $bundle_data['label'];
    }
  }

  return $bundles;
}

function cds_integration_admin_ui_fields_aliases($settings) {
  $section = array(
    '#type' => 'fieldset',
    '#title' => t('CDS Aliases'),
    '#description' => t('Configure the URL aliases for various CDS pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $section['category'] = array(
    '#type' => 'textfield',
    '#title' => t('Category page alias'),
    '#description' => t('You can use the {section}, {category}, and {category-hierarchy} placeholders.'),
    '#default_value' => $settings['aliases']['category'],
    '#size' => 100,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $section['product'] = array(
    '#type' => 'textfield',
    '#title' => t('Product page alias'),
    '#description' => t('You can use the {section}, {category}, {category-hierarchy}, and {product} placeholders.'),
    '#default_value' => $settings['aliases']['product'],
    '#size' => 100,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $section['cart'] = array(
    '#type' => 'textfield',
    '#title' => t('Cart page alias'),
    '#description' => t('You can use the {section} placeholder.'),
    '#default_value' => $settings['aliases']['cart'],
    '#size' => 100,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $section['compare'] = array(
    '#type' => 'textfield',
    '#title' => t('Compare page alias'),
    '#description' => t('You can use the {section} placeholder.'),
    '#default_value' => $settings['aliases']['compare'],
    '#size' => 100,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  // Name it "Search" because Keys doesn't really make sense in the Drupal context.
  $section['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search page alias'),
    '#description' => t('You can use the {section} placeholder.'),
    '#default_value' => $settings['aliases']['search'],
    '#size' => 100,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  return $section;
}

function cds_integration_admin_ui_fields_overrides($settings) {
  $section = array(
      '#type' => 'fieldset',
      '#title' => t('Overrides'),
      '#description' => t('Configure settings for overriding values of CDS entities'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

  $section['category_override_entity_type'] = array(
      '#type' => 'select',
      '#title' => t('Category override entity type'),
      '#description' => t('Select the entity type to use for CDS category overrides.'),
      '#options' => cds_integration_admin_ui_entity_type_options(),
      '#default_value' => $settings['overrides']['category']['entityType'],
      '#required' => FALSE,
      '#empty_value' => '',
  );

  $section['category_override_bundle_wrapper'] = array(
      '#prefix' => '<div class="category-override-bundle-wrapper">',
      '#suffix' => '</div>',
  );

  $section['category_override_bundle_wrapper']['category_override_bundle'] = array(
      '#type' => 'select',
      '#title' => t('Category override bundle'),
      '#description' => t('Select the bundle to use for CDS category overrides.'),
      '#options' => cds_integration_admin_ui_bundle_options($settings['overrides']['category']['entityType']),
      '#default_value' => $settings['overrides']['category']['bundle'],
      '#required' => FALSE,
      '#empty_value' => '',
  );

  $section['category_override_fields_wrapper'] = cds_integration_admin_ui_override_fields($settings);

  return $section;
}

function cds_integration_admin_ui_override_fields($settings) {
  $section = array(
      '#prefix' => '<div class="category-override-fields-wrapper">',
      '#suffix' => '</div>',
  );

  $fields = array(
      'headline' => 'Headline',
      'image' => 'Image',
      'description' => 'Description',
      'teaser_image' => 'Teaser image',
      'teaser_description' => 'Teaser description',
  );

  foreach ($fields as $cdsField => $cdsFieldLabel) {
    $section[$cdsField] = cds_integration_admin_ui_override_field($cdsField, $cdsFieldLabel, $settings);
  }

  return $section;
}

function cds_integration_admin_ui_override_field($cdsField, $cdsFieldLabel, $settings) {
  $bundleFields = field_info_instances($settings['overrides']['category']['entityType'], $settings['overrides']['category']['bundle']);

  $field_options = array();

  foreach ($bundleFields as $field_name => $bundle_field) {
    $field_options[$field_name] = t($bundle_field['label']);
  }

  $field = array(
      '#type' => 'select',
      '#title' => t($cdsFieldLabel . ' override field'),
      '#description' => t('Select the field to use to override the CDS "' . $cdsField . '" field, or leave blank to not override.'),
      '#options' => $field_options,
      '#default_value' => $settings['overrides']['category']['fields'][$cdsField],
      '#required' => FALSE,
      '#empty_value' => '',
  );

  return $field;
}

function cds_integration_admin_ui_fields_product_sections($settings) {
  $section = array(
    '#type' => 'fieldset',
    '#title' => t('Product Sections'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $section['contentTypes'] = array(
    '#markup' => '<p>' . t('To create product sections, simply create a CDS Product Section field on any content type, then create a new node of that type, selecting the appropriate CDS root category. There is no global configuration required.') . '</p>',
  );

  return $section;
}

function cds_integration_admin_ui_content_types_select_options() {
  $options = &drupal_static(__FUNCTION__);
  if (!isset($options)) {
    $content_types = node_type_get_types();
    $options = array();

    foreach ($content_types as $id => $type) {
      $options[$id] = $type->name;
    }
  }

  return $options;
}

function cds_integration_admin_ui_form_submit($form, &$form_state) {
  $settings = cds_integration_settings();

  $settings['host'] = $form_state['values']['general']['host'];
  $settings['domain'] = $form_state['values']['general']['domain'];
  $settings['unitSystem'] = $form_state['values']['general']['unitSystem'];
  $settings['customScript'] = $form_state['values']['general']['customScript'];
  $settings['customStylesheet'] = $form_state['values']['general']['customStylesheet'];
  $settings['cartThankYouUrl'] = $form_state['values']['general']['cartThankYouUrl'];

  $settings['aliases'] = array(
    'category' => $form_state['values']['aliases']['category'],
    'product' => $form_state['values']['aliases']['product'],
    'cart' => $form_state['values']['aliases']['cart'],
    'compare' => $form_state['values']['aliases']['compare'],
    'search' => $form_state['values']['aliases']['search'],
  );

  $settings['overrides']['category'] = array(
    'entityType' => $form_state['values']['overrides']['category_override_entity_type'],
    'bundle' => $form_state['values']['overrides']['category_override_bundle_wrapper']['category_override_bundle'],
  );

  foreach (array('headline', 'image', 'description', 'teaser_image', 'teaser_description') as $field) {
    $settings['overrides']['category']['fields'][$field] = $form_state['values']['overrides']['category_override_fields_wrapper'][$field];
  }

  variable_set('cds_integration', $settings);
}
