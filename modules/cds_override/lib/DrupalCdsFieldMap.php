<?php
use TopFloor\Cds\CdsFieldMaps\CdsFieldMap;

/**
 * Created by PhpStorm.
 * User: BMcClure
 * Date: 2/10/2016
 * Time: 10:26 PM
 */
class DrupalCdsFieldMap extends CdsFieldMap {
    protected function populateFields() {
        if (empty($this->cdsType) || empty($this->cdsId)) {
            return;
        }

        $cache = $this->service->getCache();

        $config = $this->service->getConfig();

        $overrides = $config->get('overrides');

        if (!isset($overrides[$this->cdsType])) {
            return;
        }

        $override = $overrides[$this->cdsType];

        if (empty($override['entityType']) || empty($override['bundle'])) {
            return;
        }

        $this->fieldMap = array_filter($override['fields']);

        if (empty($this->fieldMap)) {
            return;
        }

        $fields = cds_reference_fields($this->cdsType, $override['entityType'], $override['bundle']);

        if (empty($fields)) {
            return;
        }

        $cacheKey = 'cds-integration-field-map-' . $this->cdsType . '-' . $this->cdsId;

        if ($cache->exists($cacheKey)) {
            $this->fields = $cache->get($cacheKey);

            return;
        }

        $newFields = array();

        foreach (array_keys($fields) as $referenceFieldName) {
            $query = new EntityFieldQuery();
            $query->entityCondition('entity_type', $override['entityType'])
                ->entityCondition('bundle', $override['bundle'])
                ->fieldCondition($referenceFieldName, 'cds_id', $this->cdsId, '=');

            $result = $query->execute();

            if (empty($result[$override['entityType']])) {
                continue;
            }

            $eids = array_keys($result[$override['entityType']]);

            $first_eid = array_pop($eids);

            $entity = entity_load_single($override['entityType'], $first_eid);

            foreach ($this->fieldMap as $overrideField => $fieldName) {
                $value = $this->getOverrideValue($override, $entity, $fieldName, $overrideField);

                if ($value) {
                    $newFields[$fieldName] = $value;
                }
            }

            break;
        }

        $cache->set($cacheKey, $newFields);

        $this->fields = $newFields;
    }

    protected function getOverrideValue($override, $entity, $fieldName, $overrideField) {
        $items = field_get_items($override['entityType'], $entity, $fieldName);

        $result = '';

        if (isset($items[0])) {
            $imageStyle = $this->imageStyleFor($overrideField);

            if ($imageStyle) {
                if (!empty($items[0]['uri'])) {
                    $result = $this->getImageUrl($items[0]['uri'], $override, $fieldName, $imageStyle);
                }
            } elseif (!empty($items[0])) {
                $result = drupal_render(field_view_value($override['entityType'], $entity, $fieldName, $items[0]));
            }
        }

        return $result;
    }

    protected function imageStyleFor($fieldName) {
        $imageStyles = array(
            'image' => 'full',
            'teaser_image' => 'teaser',
        );

        if (array_key_exists($fieldName, $imageStyles)) {
            return $imageStyles[$fieldName];
        }

        return false;
    }

    protected function getImageUrl($uri, $override, $fieldName, $display = 'full') {
        if (!empty($uri)) {
            $imageStyle = $this->getImageStyle($override, $fieldName, $display);

            if ($imageStyle) {
                $uri = image_style_url($imageStyle, $uri);
            } else {
                $uri = file_create_url($uri);
            }
        }

        return $uri;
    }

    protected function getImageStyle($override, $fieldName, $display = 'full') {
        $field_instance = field_info_instance($override['entityType'], $fieldName, $override['bundle']);

        if (empty($field_instance)) {
            return '';
        }

        if (!isset($field_instance['display'][$display])) {
            $display = 'default';
        }

        if (!isset($field_instance['display'][$display]['settings']['image_style'])) {
            return '';
        }

        return $field_instance['display'][$display]['settings']['image_style'];
    }
}
