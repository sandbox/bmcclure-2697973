<?php
use TopFloor\Cds\CdsCollections\CdsCollection;


/**
 * A quick theme that does not use the theme() layer for menu html.
 */
class cds_product_section_MenuTheme_CdsProductSection extends menupoly_MenuTheme_Static {
  protected $categories;

  protected $service;

  function __construct() {
    $this->service = cds_integration_service();

    $this->categories = CdsCollection::create('categories', $this->service)->getItems();
  }

  /**
   * {@inheritdoc}
   */
  function renderMenuItem($item, $options, $submenu_html) {
    $link_html = l($item['title'], $item['href'], $options);
    $attr = $this->_itemAttributes($item, $options, $submenu_html);

    if (cds_product_section_menu_link_is_section($item['mlid'])) {
      $submenu_html = $this->renderProductSectionMenu($item);
    }

    return $attr->renderTag('li', $link_html . $submenu_html);
  }

  protected function renderProductSectionMenu($item) {
    $basePath = drupal_get_path_alias($item['link_path']);

    $cache = $this->service->getCache();
    $cacheKey = 'cds-product-section-menu-' . $basePath;

    if ($cache->exists($cacheKey)) {
      return $cache->get($cacheKey);
    }

    /** @var DrupalCdsUrlHandler $urlHandler */
    $urlHandler = $this->service->getUrlHandler();

    $baseCategory = $urlHandler->getEnvironmentCategoryId($basePath);

    $output = $this->renderChildMenu($baseCategory, $basePath);

    $cache->set($cacheKey, $output);

    return $output;
  }

  protected function renderProductSectionMenuItem($item, $basePath) {
    return $this->renderMenuItem($item, array(), $this->renderChildMenu($item['id'], $basePath));
  }

  protected function renderChildMenu($parentId, $basePath) {
    $children = $this->getChildCategories($parentId, $basePath);

    if (count($children) == 0) {
      return '';
    }

    foreach ($children as $id => $child) {
      $children[$id] = $this->renderProductSectionMenuItem($child, $basePath);
    }

    return $this->renderMenuTree($children);
  }

  protected function getChildCategories($parentId, $basePath) {
    $children = array();

    foreach ($this->categories as $id => $category) {
      if ($category['parent'] == $parentId) {
        $children[$id] = $this->getMenuItemForCategory($category, $basePath);
      }
    }

    return $children;
  }

  protected function categoryHasChildren($categoryId) {
    foreach ($this->categories as $id => $category) {
      if ($category['parent'] == $categoryId) {
        return true;
      }
    }

    return false;
  }

  protected function getMenuItemForCategory($category, $basePath) {
    /** @var DrupalCdsUrlHandler $urlHandler */
    $urlHandler = $this->service->getUrlHandler();

    $href = $urlHandler->construct(array('page' => 'search', 'cid' => $category['id']), null, $basePath);

    $currentUri = $urlHandler->getCurrentUri();

    $active = ($href == $currentUri || (strlen($currentUri > strlen($href)) &&
        substr($currentUri, 0, strlen($href)) == $href));

    $menuItem = array(
        'id' => $category['id'],
        'title' => $category['label'],
        'href' => $href,
        'class' => '',
        'has_children' => $this->categoryHasChildren($category['id']),
        'active' => $active,
        'active-trail' => $active,
    );

    return $menuItem;
  }
}
